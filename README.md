# TP – Cookies, Tracking & anti-tracking
G. BRUGIÈRE, T. LAHOUHEL, A. MULLER, R. VERGNE

L’objectif de ce TP est de mieux comprendre les cookies et d’approfondir le règlement du RGPD.

Afin de réaliser ce TP, vous devez forker le projet, afin de travailler sur votre propre repository. Lorsque vous aurez fini, vous nous donnerez accès à celui-ci. Merci de renommer le projet comme suit : `TP Cookies - Prénom NOM`\
Vous pouvez répondre directement sur votre `README.md`.

Prérequis :
- git
- un navigateur
- php > 7.3

Pensez à clone le projet pour travailler dessus.

## Introduction : Fabriquer un cookie (15 points)
Ingrédients :
- 1 œuf
- 85 g de beurre doux
- 100 g de pépites de chocolat
- 150 g de votre farine préférée
- ...

Non on plaisante, vous pouvez passer à la suite du TP. Même si on n'est pas contre quelques cookies 😁

## Introduction (la vraie 😉) : Données et cookies (1 points)
### Vos données (0.5 point)
Plus une entreprise a de données sur nous, et plus leur cookie de tracking sont efficaces. Or, aujourd’hui nous utilisons tous au moins un service d’un géant du web qui va ainsi collecter nos recherches, nos messages, … Toutes ces données formes ainsi un profil nous représentant. Cette quantité d’information est croissante, et ouvre énormément d’opportunité à ces entreprises.

Ceci est vrai aussi si vous n'avez pas de compte, grâce aux cookies de tracking. C’est pourquoi nous aimerions vous montrer la quantité de données collectées. Pour ce faire, nous allons passer par Google chez qui vous avez tous un compte, ce qui sera donc plus simple. En effet, depuis la mise en place du RGPD, les entreprises sont dans l'obligation de vous proposer une option vous permettant de télécharger les données qu’elle détient de vous. (Vous devez aussi pouvoir faire cette demande lorsque vous n’avez pas de compte sur un service, mais c’est une démarche plus longue, puisqu’il faut contacter l’entreprise. À l'UCA par exemple, vous trouverez uniquement un e-mail, mais dédié à cette demande)

La marche à suivre est la suivante :
1. Rendez-vous sur [Google](https://www.google.com/) ;
2. Sur votre profil, cliquez sur `Gérez votre compte Google` ;
3. Pour votre culture : Dans la section `Données et personnalisation` vous avez les liens `Mon activités` et `Vos trajets` (dans le bloc `Activités et trajets`) vous permettant d'avoir un coup d'œil sur ce que vous faites ;
4. Dans le bloc `Télécharger, supprimer ou planifier l'avenir de vos données` cliquer sur `Télécharger vos données` ;
5. Laissez tout sélectionné ;
6. En bas de la page, passez à l'étape suivante ;
7. Choisissez vos préférences de téléchargement puis créez l'exportation ;
8. Le temps de l'exportation, passez à la suite du TP.

Une fois exportée, taille de l'exportation :

### Vos cookies (0.5 point)
Afin que vous vous rendiez compte de la quantité de cookies installés pendant vos navigations, vous allez faire un petit tour dans vos paramètres et regarder le nombre de cookies par site.

**Pour Chrome** :  
- Paramètres
- Confidentialité et sécurité
- Cookies et autres données des sites
- Afficher l'ensemble des cookies et données de sites

**Pour Firefox** :  
- Préférences
- Vie privée et sécurité
- Cookies et données de sites
- Gérer les données
- Trier sur le nombre de cookies

**Pour Edge**
- Paramètres
- Cookies et autorisations de site
- Gérer et supprimer les cookies ...
- Afficher tous les cookies des sites
- Trier par `La plupart des cookies`

**Pour les autres**
- Vous devez avoir des options de sécurités dans les paramètres. Vous trouverez souvent ces informations ici.

Quel est le site ayant déposé le plus de cookie (Sous chrome : bonne chance) ? Combien ?\
Réponse :

## Partie 1 - Pratique (12 points)
Prérequis :
- Lancez le site en local (ex: `php -S localhost:8000`) depuis le dossier `site` ;
- Supprimer vos cookies provenant de `local` (cf intro 'Vos cookies').

1. Naviguez sur le site. N'hésitez pas à recharger le site de temps en temps (🐇🥚). Que constatez-vous ? (4 points)\
Réponse :

2. Combien de fois avez-vous rechargé le site ?\
Réponse :

3. Modifiez le code pour qu'il respecte la législation en vigueur (Indice : 4 améliorations attendues). Justifiez. (8 points)\
Justification :

## Partie 2 – Théorie (7 points)
### Questionnaire (5 points)
**1.** Comment appelle-t-on les cookies exploités par d’autres sites que le site sur lequel il a été enregistré ?
- [ ] Cookies de session
- [ ] Cookies permanents
- [ ] Cookies internes
- [ ] Cookies tiers

**2.** Quelle attaque à pour objectif de reprendre tel quel le cookie de sa victime et de le mettre dans ses propres cookies afin de se faire passer pour lui ?\
Réponse  :

**3.** Quelles sont les deux raisons principales pour lesquelles les acteurs du web utilisent le tracking ?\
Réponse  :

**4.** Quel est la principale plateforme utilisant des cookies pour réaliser des statistiques sur un site ?\
Réponse  :

**5.** À quelle échelle les compétences de la CNIL s’applique-t-elle ?
- [ ] France
- [ ] Europe
- [ ] Monde
- [ ] Pays Francophone

**6.** Citez au moins 2 des principaux droits apportés par la loi du 6 janvier 1978 concernant les droits des utilisateurs.\
Réponse :

**7.** Quelle réglementation permet d’harmoniser la protection au sein de l’Union Européenne ?\
Réponse :

**8.** Donnez l’équivalent de la CNIL dans un autre pays.\
Réponse :

**9.** Quelle fonction PHP permet de créer un Cookie ?\
Réponse :

**10.** Quel est la bonne syntaxe pour créer un cookie en JavaScript ?
- [ ] document.cookie = "username = John Doe; expires = Thu, 18 Dec 2013 12:00:00 UTC";
- [ ] Document.Cookie = "username = John Doe; expires = Thu, 18 Dec 2013 12:00:00 UTC";
- [ ] document.cookie = "username : ’John Doe’; expires : ’Thu, 18 Dec 2013 12:00:00 UTC’ ";
- [ ] document.cookie = "{‘username’ = ‘John Doe’, ‘expires’ = ‘Thu, 18 Dec 2013 12:00:00 UTC’}";

### Cas d'usage (2 points)
VRAI ou FAUX ? Justifiez votre réponse dans tous les cas.

#### 1.
L'INSEE à besoin de recenser la population Française afin de pouvoir donner un nombre précis de résidents en France.
Pour cela, l'INSEE récupère vos informations personnelles sans votre consentement, et même sans que vous en ayant connaissance.
Dans cette situation, l'INSEE enfreint le RGPD.
- [ ] VRAI
- [ ] FAUX

Justification :

#### 2.
Monsieur X possède un site e-commerce lui permettant de vendre ses produits.\
Lors de la création du compte client, Monsieur X informe ses utilisateurs de la nécessité de recueillir certaines informations telles que : le prénom, le nom, l'âge et le sexe de la personne.\
Avec le consentement de l'utilisateur, l'adresse postale ainsi que les numéros de carte bancaire peuvent être enregistrés pour une prochaine commande.\
Malheureusement, une base données contenant toutes ces informations a fuité et Monsieur X se voit dans l'obligation de prévenir la CNIL.\
Heureusement, les adresses postales et cartes bancaires des utilisateurs sont stockées dans une autre base de données et n'ont donc pas étaient impactées par la fuite. Étant donné que ces données personnelles en question n'ont pas fuitées, Monsieur X n'est à priori pas dans l'obligation d'en informer les utilisateurs.
Cependant, Monsieur X décide tout de même d'informer les utilisateurs de son site de la fuite.
Monsieur X a choisi la bonne option.
- [ ] VRAI
- [ ] FAUX

Justification :

---
## Pensez à push votre code et à nous avertir lorsque vous avez terminé !
