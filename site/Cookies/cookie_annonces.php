<?php if(isset($index_key) && $index_key == "123") {

    $annonces_user = (isset($_COOKIE['annonces_user'])) ? json_decode($_COOKIE['annonces_user']) : [];

    if (isset($_POST['annonces'])) {
        array_push($annonces_user, json_decode($_POST['annonces']));

        $options = [
            'expires' => time() + 3600 * 24 * 30, // 30 jours
            'path' => '/',
            'domain' => (isset($domaine)) ? $domaine : "localhost",
            'secure' => true,
            'httponly' => false,
            'samesite' => "None"
        ];

        setcookie('annonces_user', json_encode($annonces_user), $options);
    }
} else {
    header('Location:/');
}