<?php if(isset($index_key) && $index_key == "123") {

    $theme = "lightTheme.css";

    if(isset($_COOKIE['theme'])){
        if($_COOKIE['theme'] == 'dark'){
            $theme = "darkTheme.css";
        }
    }

    if(isset($_POST["theme"])){
        $theme = (($_POST["theme"] == "dark")?"darkTheme.css":"lightTheme.css");

        $options = [
            'expires' => time() + 3600*24*30, // 30 jours
            'path' => '/',
            'domain' => (isset($domaine))?$domaine:"localhost",
            'secure' => true,
            'httponly' => false,
            'samesite' => "None"
        ];

        setcookie('theme', $_POST["theme"], $options);
    }
} else {
    header('Location:/');
}