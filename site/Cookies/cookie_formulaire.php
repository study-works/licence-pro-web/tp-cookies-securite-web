<?php if(isset($index_key) && $index_key == "123") {

    if(isset($_POST['form_acc']) && $_POST['form_acc'] == 'ok'){

        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $mdp = $_POST['mdp'];
        $email = $_POST['email'];

        $values = [$nom, $prenom, $mdp, $email, date("d/m/Y H:i:s")];

        $options = [
            'expires' => time() + 3600*24*365*100,
            'path' => '/',
            'domain' => (isset($domaine))?$domaine:"localhost",
            'secure' => false,
            'httponly' => false,
            'samesite' => "Lax"
        ];

        setcookie('data_form', json_encode($values), $options);
    }
} else {
    header('Location:/');
}