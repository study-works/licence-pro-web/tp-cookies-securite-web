<?php if(isset($index_key) && $index_key == "123") {

    if(!isset($_COOKIE['heure'])) {

        $options = [
            'expires' => time() + 3600 * 24 * 800, // 800 jours
            'path' => '/',
            'domain' => (isset($domaine)) ? $domaine : "localhost",
            'secure' => true,
            'httponly' => false,
            'samesite' => "None"
        ];

        setcookie('heure', date("d/m/Y H:i:s"), $options);
    }
} else {
    header('Location:/');
}