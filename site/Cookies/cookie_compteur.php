<?php if(isset($index_key) && $index_key == "123") {

    $nb_reload = (isset($_COOKIE['nb_reload']))?intval($_COOKIE['nb_reload']):0; // si isset => $_COOKIES... sinon 0

    $options = [
        'expires' => time() + 3600*24*364,
        'path' => '/',
        'domain' => (isset($domaine))?$domaine:"localhost",
        'secure' => true,
        'httponly' => false,
        'samesite' => "None"
    ];

    $nb_reload += 1;

    setcookie('nb_reload', $nb_reload, $options);
} else {
    header('Location:/');
}