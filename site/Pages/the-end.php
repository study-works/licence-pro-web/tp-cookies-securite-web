<?php if(isset($index_key) && $index_key == "123"){

    $infos = (isset($_COOKIE['data_form'])) ? json_decode($_COOKIE['data_form']) : [];
    $nbReload = (isset($_COOKIE['nb_reload'])) ? $_COOKIE['nb_reload'] : "";
    $adverts = (isset($_COOKIE['annonces_user'])) ? json_decode($_COOKIE['annonces_user']) : [];
    $heure = (isset($_COOKIE['heure'])) ? $_COOKIE['heure'] : "";
?>

<h1>Les Cookies sont chauds !</h1>

<?php
if (isset($infos) && isset($nbReload) && isset($adverts)) {
?>
<p>Bravo <?=$infos[1]?> !!</p>
<p>J'aime beaucoup ton mot de passe : <strong><?=$infos[2]?></strong>. J'espère que toi non, tu vas pouvoir le changer 😁</p>
<p>Date et heure de première connexion : <?=$heure?></p>
<p><strong>Au fait, tu étais au courant que tu avais rechargé le site <?=$nbReload?> fois ?</strong> Moi en tout cas je trouve ça cool de le savoir pour mes statistiques 🙂</p>
<p>Ah oui, et ça se sont les articles sur lesquels tu as cliqué 🙂</p>
<ul>
    <?php foreach ($adverts as $advert) {
        foreach ($advert as $key => $value) {
        ?>
        <li><?=$key?> - <?=$value?></li>
        <?php }?>
        <br />
    <?php } ?>
</ul>
<?php } ?>
<hr/>
<p>Encore merci pour tout ça, c'est vraiment cool ! 😉</p>
<video controls width="600" autoplay="true" loop="true">
    <source src="https://media.tenor.co/videos/860678da8a769e90132443ffe37d0f5f/mp4"
            type="video/mp4">
</video>

<?php }else{
    header('Location:/');
} ?>
