<?php if(isset($index_key) && $index_key == "123"){
    $data = [];

    include_once "Resources/data.php";

    $selected_categ = (isset($_POST['categ']))?$_POST['categ']:'Vehicules';
    include_once "Cookies/cookie_annonces.php";
?>

<form method="post" id="form_categ">
    <input type="hidden" value="Vehicules" name="categ" id="categ"/>
    <input type="hidden" value="annonces" name="page"/>
</form>

<p>Vous pouvez choisir une catégorie :</p>
<div class="categs">

    <?php foreach ($data as $nom_categ => $categ){ ?>
        <span class="categ" onclick="document.getElementById('categ').value = '<?=$nom_categ?>'; document.getElementById('form_categ').submit();"><?=$nom_categ?></span>
    <?php } ?>

</div>



<form method="post" id="form_annonces">
    <input type="hidden" value="<?=$selected_categ?>" name="categ" id="categ"/>
    <input type="hidden" value="" name="annonces" id="annonces"/>
    <input type="hidden" value="annonces" name="page"/>
</form>

<p>Vous pouvez cliquer sur quelque annonces :</p>
<table class="ads">
    <?php foreach ($data[$selected_categ] as $nom_type => $type) { ?>
        <?php foreach ($type as $prod){ ?>
            <?php   $prod_json = str_replace('"', '\"', json_encode($prod)); ?>
            <tr class="ad" onclick='document.getElementById("annonces").value = "<?=$prod_json?>"; document.getElementById("form_annonces").submit();'>
                <?php if($nom_type != 'Jeux'){ ?>

                    <td width="200"><?=$prod['marque']?></td>
                    <td width="200"><?=$prod['model']?></td>
                    <td width="200"><?=$prod['couleur']?></td>

                <?php }else{ ?>

                    <td width="200"><?=$prod['titre']?></td>

                <?php } ?>
            </tr>
        <?php } ?>
    <?php }?>
</table>

<?php }else{
    header('Location:/');
} ?>
