<?php
    ini_set('display_errors', 0);

    $index_key = "123";
    $citations = [];

    date_default_timezone_set('Europe/Paris');

    $domaine = 'localhost';

    include_once "Cookies/cookie_compteur.php";
    include_once "Cookies/cookie_theme.php";
    include_once "Cookies/cookie_heure.php";
    include_once "Resources/citations.php";

    $nbReload = (isset($_COOKIE['nb_reload'])) ? $_COOKIE['nb_reload'] : [];

    $page = (isset($_GET['page']))?$_GET['page']:'formulaire';
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <link href="Style/style.css" rel="stylesheet">
        <link href="Style/<?=(isset($theme))?$theme:'lightTheme.css'?>" rel="stylesheet">
        <title>Site sur les cookies</title>
    </head>
    <body>
        <header id="navbar">
            <a href="?page=formulaire" class="nav-link">Formulaire</a>
            <?php if (isset($_COOKIE['data_form']) || isset($_POST['form_acc']) ) { ?>
                <a href="?page=theme" class="nav-link">Theme</a>
                <a href="?page=annonces" class="nav-link">Annonces</a>
                <?php if ($nbReload > 7) { ?>
                    <a href="?page=the-end" class="nav-link">Fin</a>
                <?php }?>

                <p class="display-theme">Votre thème : <?php echo $_COOKIE['theme'] ?? "Non renseigné" ?></p>
            <?php }?>
        </header>

        <main>
            <?php include_once "Pages/".$page.".php" ?>
        </main>

        <footer>
            <p><?php
                $k = array_rand($citations);
                echo $citations[$k];
                ?></p>
        </footer>
    </body>
</html>
