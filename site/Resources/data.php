<?php if(isset($index_key) && $index_key == "123") {
    $data = [
        'Vehicules' =>
            [
                'Voitures' => [
                    [
                        'marque' => 'Renault',
                        'model' => '4L',
                        'couleur' => 'Jaune'
                    ],
                    [
                        'marque' => 'Volkswagen',
                        'model' => 'Combi T2',
                        'couleur' => 'Bleu'
                    ],
                    [
                        'marque' => 'Citroen',
                        'model' => '2CV',
                        'couleur' => 'Gris'
                    ]
                ],

                'Motos' => [
                    [
                        'marque' => 'Yamaha',
                        'model' => 'MT-09',
                        'couleur' => 'Bleu'
                    ],
                    [
                        'marque' => 'Triumph',
                        'model' => 'Trident 660',
                        'couleur' => 'Noir'
                    ],
                ],

            ],

        'Musique' =>
            [
                'Guitares' => [
                    [
                        'marque' => 'Fender',
                        'model' => '60 Strat FADB Relic',
                        'couleur' => 'Bleu'
                    ],
                    [
                        'marque' => 'Gibson',
                        'model' => 'ES-339 Figured 60s Cherry',
                        'couleur' => 'Rouge'
                    ]

                ],

                'Pianos' => [
                    [
                        'marque' => 'Steinway & Sons',
                        'model' => 'B-211',
                        'couleur' => 'Noir'
                    ],
                    [
                        'marque' => 'Casio',
                        'model' => 'PX-870 WE Privia',
                        'couleur' => 'Blanc'
                    ],
                    [
                        'marque' => 'Yamaha',
                        'model' => 'C7X SH2 PE',
                        'couleur' => 'Noir'
                    ],
                ],

            ],

        'Gaming' =>
            [
                'Consoles' => [
                    [
                        'marque' => 'Sony',
                        'model' => 'PS4',
                        'couleur' => 'Noir'
                    ],
                    [
                        'marque' => 'Microsoft',
                        'model' => 'XBox One',
                        'couleur' => 'Noir'
                    ],
                    [
                        'marque' => 'Nintendo',
                        'model' => 'Switch',
                        'couleur' => 'Rouge & Bleu'
                    ],

                ],

                'Jeux' => [
                    [
                        'titre' => 'The last of us',
                    ],
                    [
                        'titre' => 'GTA5',
                    ],
                    [
                        'titre' => 'Minecraft',
                    ],
                    [
                        'titre' => 'BioShock',
                    ],
                ]

            ]
    ];
} else {
    header('Location:/');
}