<?php if(isset($index_key) && $index_key == "123") {
    $citations = [
        "Les cookies sont chauds. 🍪",
        "Un cookie ne doit pas cuire plus d'1 an.",
        "C'est bon les cookies, alors garde les pour toi.",
        "Fais bien attention à tes cookies. Qui sait ce que je pourrais faire avec ? 😈",
        "J'aime bien quand on me demande si j'en veux.",
        "Les cookies vous enterreront tous ! 🪦",
        "Security is a process, not a product. - Bruce Schneier",
        "Once you have something on the Internet, you are tellingthe world, please come hack me. - Ron Rivest",
        "Fully secure systems don’t exist today and they won’t existin the future. - Adi Shamir",
        "If privacy is outlawed, only outlaws will have privacy - Phil Zimmermann",
        "Your rights matter, because you never know when you’regoing to need them. - Edward Snowden",
        "Arguing that you don’t care about privacy because youhave nothing to hide is no different than saying you don’tcare about free speech because you have nothing to say. - Edward Snowden",
        "If you think technology can solve your security problems,then you don’t understand the problems and you don’tunderstand the technology. - Bruce Schneier",
        "Don’t be evil - Google",
        "Cookiiiiies !!",
    ];
} else {
    header('Location:/');
}